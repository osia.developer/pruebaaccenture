package co.osiadeveloper.pruebaaccenture.data;

import org.dizitart.no2.Nitrite;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class Database implements IDatabase {

    private static Nitrite db;
    private static Database instance;

    @Bean
    @Scope("singleton")
    public static synchronized Database getInstance() {
        if (instance == null) {
            instanceDB();
            instance = new Database();
        }
        return instance;
    }


    private static void instanceDB() {
        String temp = System.getProperty("java.io.tmpdir");
        db = Nitrite.builder()
                .compressed()
                .filePath( temp + "/test.db")
                .openOrCreate("user", "password");
    }

    public Nitrite getDb() {
        return db;
    }

}
