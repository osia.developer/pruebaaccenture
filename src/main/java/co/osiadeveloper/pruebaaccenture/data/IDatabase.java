package co.osiadeveloper.pruebaaccenture.data;

import org.dizitart.no2.Nitrite;

public interface IDatabase {

    Nitrite getDb();

}
