package co.osiadeveloper.pruebaaccenture.service;

import co.osiadeveloper.pruebaaccenture.controller.IContactsController;
import co.osiadeveloper.pruebaaccenture.model.Contacts;
import co.osiadeveloper.pruebaaccenture.repository.contacts.IContactsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ContactsServiceImpl implements IContactsController {

    @Autowired
    IContactsRepository meetingRepository;

    @Override
    public List findAll() {
        try {
            return meetingRepository.findAll();
        } catch (Exception e) {
            //logger.error(e.getMessage());
            throw e;
        }
    }

    @Override
    public boolean save(Contacts c) {
        try {
            return meetingRepository.save(c);
        } catch (Exception e) {
            //logger.error(e.getMessage());
            throw e;
        }
    }

}
