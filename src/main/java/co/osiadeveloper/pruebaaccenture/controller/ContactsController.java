package co.osiadeveloper.pruebaaccenture.controller;

import co.osiadeveloper.pruebaaccenture.model.Contacts;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController
@RequestMapping("/meeting")
@Api(value = "/meeting", description = "Servicio para consulta y registro de meetings del sistema")
public class ContactsController {

    @Autowired
    private IContactsController iActionController;

    @RequestMapping(value = "/findAll", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
    @ApiOperation(value = "findAll", notes = "Descripcion del servicio")
    @CrossOrigin(origins = "*")
    //@Transactional
    public List<Contacts> findAll() {
        List<Contacts> lst = iActionController.findAll();
        return lst;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON,produces = MediaType.APPLICATION_JSON)
    @ApiOperation(value = "save", notes = "Consulta la informacion de los funcionarios del sistema")
    @CrossOrigin(origins = "*", allowedHeaders= {"application/json"})
    public boolean save(@RequestBody Contacts contacts) throws Exception {

        try {
            return iActionController.save(contacts);
        }
        catch (Exception e) {

            return false;
        }
        //ValidationUtil.validate(etapaDTO);
    }

}
