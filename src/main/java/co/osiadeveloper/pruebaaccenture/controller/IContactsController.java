package co.osiadeveloper.pruebaaccenture.controller;

import co.osiadeveloper.pruebaaccenture.model.Contacts;

import java.util.List;

public interface IContactsController<T> {

    List<T> findAll();

    boolean save(Contacts c);

}
