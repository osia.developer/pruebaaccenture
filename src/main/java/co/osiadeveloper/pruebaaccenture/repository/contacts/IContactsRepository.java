package co.osiadeveloper.pruebaaccenture.repository.contacts;

import co.osiadeveloper.pruebaaccenture.model.Contacts;

import java.util.List;

public interface IContactsRepository {

    List<Contacts> findAll();

    boolean save(Contacts c);

}
