package co.osiadeveloper.pruebaaccenture.repository.contacts;

import co.osiadeveloper.pruebaaccenture.data.IDatabase;
import co.osiadeveloper.pruebaaccenture.model.Contacts;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ContactsRepository implements IContactsRepository {

    @Qualifier("getInstance")
    @Autowired
    IDatabase iDatabase;

    @Override
    public List<Contacts> findAll() {
        ObjectRepository<Contacts> repository = iDatabase.getDb().getRepository(Contacts.class);
        Cursor<Contacts> cursor = repository.find();

        return cursor.toList();
    }

    @Override
    public boolean save(Contacts c) {
        ObjectRepository<Contacts> repository = iDatabase.getDb().getRepository(Contacts.class);
        repository.insert(c);
        return true;
    }
}
